import classes from './MealsSummary.module.css';

export default function MealsSummary(){
	return <section className={classes.summary}>
		<h2>Fashion, Delivered To You</h2>
		<p>
			Choose your favorite fashion item from our broad selection of available items 
			and feel like a star in the spotlight.
		</p>
		<p>
			All our items are high-quality, just in time for the fashion get away!
		</p>
	</section>
}